package com.maptest.csie.CSIE;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ArrayAdapterActivity extends AppCompatActivity {
    private ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_array_adapter);

        listView = new ListView(this);
        listView.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_expandable_list_item_1,getData()));
        setContentView(listView);
    }

    private List<String> getData(){
        List<String> testdata = new ArrayList<String>();
        testdata.add("AAAAA");
        testdata.add("BBBBB");
        testdata.add("CCCCC");
        return testdata;
    }
}
