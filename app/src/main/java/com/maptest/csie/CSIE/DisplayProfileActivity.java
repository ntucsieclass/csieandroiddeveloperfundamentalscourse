package com.maptest.csie.CSIE;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

public class DisplayProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_phone);

        Intent intent = getIntent();
        String outname = intent.getStringExtra(IOActivity.EXTRA_NAME);
        String outphone = intent.getStringExtra(IOActivity.EXTRA_PHONE);
        String outemail = intent.getStringExtra(IOActivity.EXTRA_EMAIL);
        Log.d("[@@ OUTPUT @@]",outphone);
        TextView nametextView = (TextView)findViewById(R.id.nameTextView);
        nametextView.setText(outname);
        TextView phonetextView = (TextView)findViewById(R.id.phoneTextView);
        phonetextView.setText(outphone);
        TextView emailtextView = (TextView)findViewById(R.id.emailTextView);
        emailtextView.setText(outemail);
    }
}
