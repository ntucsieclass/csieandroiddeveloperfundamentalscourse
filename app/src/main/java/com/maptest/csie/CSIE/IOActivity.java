package com.maptest.csie.CSIE;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class IOActivity extends AppCompatActivity {
    public static final String EXTRA_NAME = "com.maptest.csie.hw.NAME";
    public static final String EXTRA_PHONE = "com.maptest.csie.hw.PHONE";
    public static final String EXTRA_EMAIL = "com.maptest.csie.hw.EMAIL";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_io);
    }

    public void sendPhone (View view){
        EditText nameText = (EditText)findViewById(R.id.nameEditText);
        EditText phoneText = (EditText)findViewById(R.id.phoneEditText);
        EditText emailText = (EditText)findViewById(R.id.emailEditText);
        String name = nameText.getText().toString();
        String phone = phoneText.getText().toString();
        String email = emailText.getText().toString();
        Log.d("[@@ INPUT @@]",phone);

        Intent intent = new Intent(this,DisplayProfileActivity.class);
        intent.putExtra(EXTRA_NAME,name);
        intent.putExtra(EXTRA_PHONE,phone);
        intent.putExtra(EXTRA_EMAIL,email);
        startActivity(intent);
    }
}
