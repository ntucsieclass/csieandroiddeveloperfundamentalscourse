package com.maptest.csie.CSIE;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button mediaBtn = (Button)findViewById(R.id.media_btn);
        mediaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,MediaActivity.class);
                startActivity(intent);
            }
        });
    }

    public void clickHwBtn (View view)
    {
        Intent intent = new Intent(this,HWActivity.class);
        startActivity(intent);
    }

    public void clickIOBtn (View view)
    {
        Intent intent = new Intent(this,IOActivity.class);
        startActivity(intent);
    }

    public void clickContactBtn (View view)
    {
        Intent intent = new Intent(this,ContactActivity.class);
        startActivity(intent);
    }

    public void clickArrayAdapterBtn (View view)
    {
        Intent intent = new Intent(this,ArrayAdapterActivity.class);
        startActivity(intent);
    }

    public void clickSimpleAdapterBtn (View view)
    {
        Intent intent = new Intent(this,SimAdpActivity.class);
        startActivity(intent);
    }

    public void clickSimpleCursorAdapterBtn (View view)
    {
        Intent intent = new Intent(this,SimCurAdpActivity.class);
        startActivity(intent);
    }

    public void clickSimpleAdapterPlusBtn (View view)
    {
        Intent intent = new Intent(this,SimAdpPlusActivity.class);
        startActivity(intent);
    }

    public void clickMusicBtn (View view)
    {
        Intent intent = new Intent(this,MusicActivity.class);
        startActivity(intent);
    }

    public void clickRotationBtn (View view)
    {
        Intent intent = new Intent(this,RotationActivity.class);
        startActivity(intent);
    }

    public void clickCount (View view)
    {
        Intent intent = new Intent(this,TimerActivity.class);
        startActivity(intent);
    }

    public void clickMyService (View view)
    {
        Intent intent = new Intent(this,MyServiceActivity.class);
        startActivity(intent);
    }

    public void clickCalculator (View view)
    {
        Intent intent = new Intent(this,CalculatorActivity.class);
        startActivity(intent);
    }
}
