package com.maptest.csie.CSIE;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MediaActivity extends AppCompatActivity {
    private MediaPlayer mediaPlayer;
    private Button spcBtn;
    private boolean needPlay = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media);
        spcBtn = (Button)findViewById(R.id.spc_btn);
        mediaPlayer = MediaPlayer.create(this,R.raw.take_me_to_the_depths);
        Toast.makeText(this,"onCreate",Toast.LENGTH_SHORT).show();
    }

    public void startBtn(View view){
        if(mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            spcBtn.setText("Continue");
        }
        else{
            mediaPlayer.start();
            spcBtn.setText("Pause");
        }
    }

    public void stopBtn(View view){
        mediaPlayer.seekTo(0);
        mediaPlayer.pause();
        spcBtn.setText("Start");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(this,"onStart",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Toast.makeText(this,"onStop",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this,"onDestroy",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if(needPlay) {
            mediaPlayer.start();
            spcBtn.setText("Pause");
        }
        Toast.makeText(this,"onRestart",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            needPlay = true;
        }
        else {
            needPlay = false;
        }
        Toast.makeText(this,"onPause",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this,"onResume",Toast.LENGTH_SHORT).show();
    }
}
