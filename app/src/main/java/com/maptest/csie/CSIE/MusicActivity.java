package com.maptest.csie.CSIE;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.LinkedList;
import java.util.List;

public class MusicActivity extends AppCompatActivity {

    private MediaPlayer mediaPlayer;
    private Button spcBtn;
    private LinkedList<Integer> songList;
    private int index=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);
        spcBtn = (Button)findViewById(R.id.music_play_btn);

        songList = new LinkedList<Integer>();
        songList.add(R.raw.beach);
        songList.add(R.raw.take_me_to_the_depths);
        songList.add(R.raw.fond_memories);

        mediaPlayer = MediaPlayer.create(this,songList.get(index));
    }

    public void startBtn(View view){
        if(mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            spcBtn.setText("Continue");
        }
        else{
            mediaPlayer.start();
            spcBtn.setText("Pause");
        }
    }

    public void prevBtn(View view){
        if(index==0)
            index=songList.size()-1;
        else
            index--;

        mediaPlayer.stop();
        mediaPlayer = MediaPlayer.create(MusicActivity.this,songList.get(index));
        mediaPlayer.start();
        spcBtn.setText("Pause");




    }

    public void nextBtn(View view){

        if(index==songList.size()-1)
            index=0;
        else
            index++;

        mediaPlayer.stop();
        mediaPlayer = MediaPlayer.create(MusicActivity.this,songList.get(index));
        mediaPlayer.start();
        spcBtn.setText("Pause");
    }

    public void stopBtn(View view){
        mediaPlayer.seekTo(0);
        mediaPlayer.pause();
        spcBtn.setText("Start");
    }
}
