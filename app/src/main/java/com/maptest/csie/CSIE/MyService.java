package com.maptest.csie.CSIE;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.util.Date;

public class MyService extends Service {

    private LocalBinder mLocBin = new LocalBinder();

    public class LocalBinder extends Binder
    {
        MyService getService()
        {
            Toast.makeText(MyService.this,"LocalBinder",Toast.LENGTH_SHORT).show();
            return MyService.this;
        }
    }

    private Handler myHandler = new Handler();
    private Runnable showTime = new Runnable() {
        @Override
        public void run() {
            String  time = new Date().toString();
            Toast.makeText(MyService.this,"Time:"+time,Toast.LENGTH_SHORT).show();
            myHandler.postDelayed(this,5000);
        }
    };

    public void myBusLogic()
    {
        Toast.makeText(this,"myBusLogic",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(this,"onCreate",Toast.LENGTH_SHORT).show();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this,"onStartCommand",Toast.LENGTH_SHORT).show();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        myHandler.post(showTime);
        Toast.makeText(this,"onBind",Toast.LENGTH_SHORT).show();
        return mLocBin;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Toast.makeText(this,"onUnbind",Toast.LENGTH_SHORT).show();
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        myHandler.removeCallbacks(showTime);
        Toast.makeText(this,"onDestroy",Toast.LENGTH_SHORT).show();
    }
}
