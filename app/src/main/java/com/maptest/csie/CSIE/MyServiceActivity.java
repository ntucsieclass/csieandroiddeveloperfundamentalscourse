package com.maptest.csie.CSIE;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MyServiceActivity extends AppCompatActivity {
    private MyService myService = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_service);
    }

    private ServiceConnection myServiceConntion = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            myService = ((MyService.LocalBinder)iBinder).getService();

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Toast.makeText(MyServiceActivity.this,"onServiceDisconnected"+ componentName.getClassName(),Toast.LENGTH_SHORT).show();
        }
    };

    public void clickMyServiceStart(View view)
    {
        myService = null;
        Intent intent = new Intent(MyServiceActivity.this,MyService.class);
        startService(intent);
    }

    public void clickMyServiceStop(View view)
    {
        myService = null;
        Intent intent = new Intent(MyServiceActivity.this,MyService.class);
        stopService(intent);
    }

    public void clickMyServiceBind(View view)
    {
        myService = null;
        Intent intent = new Intent(MyServiceActivity.this,MyService.class);
        bindService(intent,myServiceConntion,BIND_AUTO_CREATE);
    }

    public void clickMyServiceUnBind(View view)
    {
        myService = null;
        unbindService(myServiceConntion);
    }

    public void clickMyServiceMyLogic(View view)
    {
        if( myService != null )
            myService.myBusLogic();
    }
}
