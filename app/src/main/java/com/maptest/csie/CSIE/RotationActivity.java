package com.maptest.csie.CSIE;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.TextView;

public class RotationActivity extends AppCompatActivity {
    private TextView rotView;
    private int counter =0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayMetrics dm = getResources().getDisplayMetrics();
        int high = dm.heightPixels;
        int width = dm.widthPixels;

        if( high > width) {
            setContentView(R.layout.activity_rotation1);
            rotView = (TextView)findViewById(R.id.rot_view1);
        }else {
            setContentView(R.layout.activity_rotation2);
            rotView = (TextView)findViewById(R.id.rot_view2);
        }
    }

    public void clickRotBtn(View view)
    {
        counter++;
        rotView.setText(String.valueOf(counter));
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        String tmp = rotView.getText().toString();
        if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){
            setContentView(R.layout.activity_rotation2);
            rotView = (TextView)findViewById(R.id.rot_view2);
//            if(!tmp.equals("直屏幕"))
                rotView.setText(tmp);
        }
        else {
            setContentView(R.layout.activity_rotation1);
            rotView = (TextView)findViewById(R.id.rot_view1);
//            if(!tmp.equals("橫屏幕"))
                rotView.setText(tmp);
        }
    }
}
