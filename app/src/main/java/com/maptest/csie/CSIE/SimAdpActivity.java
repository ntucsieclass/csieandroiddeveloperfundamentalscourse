package com.maptest.csie.CSIE;

import android.app.ListActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimAdpActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SimpleAdapter simpleAdapter = new SimpleAdapter(this,getData(),
                R.layout.activity_simple_adapter,
                new String[]{"imgKey","textKey"},
                new int[]{R.id.saImageView,R.id.saTextView});
        setListAdapter(simpleAdapter);
    }

    private List<Map<String,Object>> getData(){
        List<Map<String,Object>> browserList = new ArrayList<Map<String,Object>>();
        Map<String,Object> browserMap;

        browserMap = new HashMap<String, Object>();
        browserMap.put("imgKey",R.drawable.firefox_icon);
        browserMap.put("textKey","Firefox");
        browserList.add(browserMap);

        browserMap = new HashMap<String, Object>();
        browserMap.put("imgKey",R.drawable.google_chrome_icon);
        browserMap.put("textKey","Google");
        browserList.add(browserMap);

        browserMap = new HashMap<String, Object>();
        browserMap.put("imgKey",R.drawable.safari_icon);
        browserMap.put("textKey","Safari");
        browserList.add(browserMap);

        return browserList;
    }
}
