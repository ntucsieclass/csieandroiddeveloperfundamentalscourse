package com.maptest.csie.CSIE;

        import android.content.ContentResolver;
        import android.content.pm.PackageManager;
        import android.database.Cursor;
        import android.provider.ContactsContract;
        import android.support.annotation.NonNull;
        import android.support.v4.app.ActivityCompat;
        import android.support.v7.app.AlertDialog;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.widget.ListView;
        import android.widget.SimpleAdapter;
        import java.util.ArrayList;
        import java.util.HashMap;
        import static android.Manifest.permission.READ_CONTACTS;
        import static android.Manifest.permission.WRITE_CONTACTS;

public class SimAdpPlusActivity extends AppCompatActivity {
    public static final int PLUS_REQUEST_CONTACTS=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_adapter_plus);
        int contactPermission = ActivityCompat.checkSelfPermission(this,
                READ_CONTACTS);        if(contactPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{READ_CONTACTS,WRITE_CONTACTS},PLUS_REQUEST_CONTACTS);
        }
        else {
            readContacts();
        }
    }

    private void readContacts() {
        ContentResolver resolver = getContentResolver();
        Cursor cursor = resolver.query(
                ContactsContract.Contacts.CONTENT_URI,
                null,
                null,
                null,
                null );

        ListView listView = (ListView) findViewById(R.id.plus_contact_view);

        ArrayList<HashMap<String,String>> list2 = new ArrayList<HashMap<String,String>>();
        HashMap<String,String> item;
        while(cursor.moveToNext()){
            int id = cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            Cursor phoneCursor = resolver.query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID+"="+id,
                    null,
                    null);
            String phone = null;
            while(phoneCursor.moveToNext()) {
                phone = phoneCursor.getString(
                        phoneCursor.getColumnIndex(android.provider.ContactsContract.CommonDataKinds.Phone.NUMBER));
            }
            phoneCursor.close();
            item = new HashMap<String,String>();
            item.put( "nameKey", name);
            item.put( "phoneKey", phone);
            list2.add( item );
        }
        cursor.close();

        SimpleAdapter simpleAdapter = new SimpleAdapter(this, list2,
                android.R.layout.simple_list_item_2,
                new String[] { "nameKey","phoneKey" },
                new int[] {android.R.id.text1,android.R.id.text2});

        listView.setAdapter(simpleAdapter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch(requestCode) {
            case PLUS_REQUEST_CONTACTS:
                if(grantResults.length>0&&grantResults[0]==PackageManager.PERMISSION_GRANTED) {
                    readContacts();
                }else {
                    new AlertDialog.Builder(this)
                            .setMessage("拜託給我權限啦！")
                            .setPositiveButton("拜託啦！",null)
                            .show();
                }
                break;
        }
    }
}

