package com.maptest.csie.CSIE;

        import android.Manifest;
        import android.app.ListActivity;
        import android.content.ContentResolver;
        import android.content.pm.PackageManager;
        import android.database.Cursor;
        import android.provider.ContactsContract;
        import android.support.annotation.NonNull;
        import android.support.v4.app.ActivityCompat;
        import android.support.v7.app.AlertDialog;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.util.Log;
        import android.widget.ListView;
        import android.widget.SimpleCursorAdapter;
        import android.widget.TextView;

        import java.util.HashMap;

        import static android.Manifest.permission.READ_CONTACTS;
        import static android.Manifest.permission.WRITE_CONTACTS;

public class SimCurAdpActivity extends ListActivity {
    public static final int CURSOR_REQUEST_CONTACTS=2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_cursor_adapter);
        int contactPermission = ActivityCompat.checkSelfPermission(this,
                READ_CONTACTS);
        if(contactPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{READ_CONTACTS,WRITE_CONTACTS},CURSOR_REQUEST_CONTACTS);
        }
        else {
            readContact();
        }
    }

    private void readContact() {
        ContentResolver resolver = getContentResolver();
        Cursor cursor = resolver.query(
                ContactsContract.Contacts.CONTENT_URI,
                null,
                null,
                null,
                null );

        ListView listView = (ListView)findViewById(R.id.contact_view);
        SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(
                this,
                android.R.layout.simple_list_item_1,
                cursor,
                new String[]{ContactsContract.Contacts.DISPLAY_NAME},
                new int[]{android.R.id.text1},
                1
        );
        listView.setAdapter(simpleCursorAdapter);

//        cursor.close();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch(requestCode) {
            case CURSOR_REQUEST_CONTACTS:
                if(grantResults.length>0&&grantResults[0]==PackageManager.PERMISSION_GRANTED) {
                    readContact();
                }else {
                    new AlertDialog.Builder(this)
                            .setMessage("拜託給我權限啦！")
                            .setPositiveButton("拜託啦！",null)
                            .show();
                }
                break;
        }
    }
}

