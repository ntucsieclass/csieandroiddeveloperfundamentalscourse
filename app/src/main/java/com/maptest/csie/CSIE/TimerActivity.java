package com.maptest.csie.CSIE;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class TimerActivity extends AppCompatActivity {
    public TextView textView;
    public static Handler handler;
    Thread countThread;
    public int x = 0;
    public int y = 0;
    public String threadMessage = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_count);
        textView = (TextView)findViewById(R.id.abcView);
        textView.setText("0.0秒");
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                Log.d("[Count]", threadMessage);
                textView.setText(threadMessage);
                Toast.makeText(TimerActivity.this,threadMessage,Toast.LENGTH_SHORT).show();
            }
        };
    }

    public void clickStopCount(View view){
        countThread.interrupt();
        x=0;
        y=0;
        textView.setText("0.0秒");
    }
    public void clickPauseCount(View view){
        countThread.interrupt();
    }
    public void clickStartCount(View view){
        countThread = new CountThread();
        countThread.start();
    }

    class CountThread extends Thread{
        public void run(){
            while(true){
                try{
                    for(int i=y;i<10;i++)
                    {

                        threadMessage = x+"."+i+"秒";
                        handler.sendEmptyMessage(0);
                        Thread.sleep(100);
                        y++;
                    }
                    y=0;
                    x++;
                }catch (InterruptedException e) {
                    e.printStackTrace();
                    break;
                }
            }
        }
    }
}
